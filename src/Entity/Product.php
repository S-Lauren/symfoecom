<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Product
 *
 * @ORM\Table(name="product")
 * @ORM\Entity
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=200, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="imagelink", type="text", length=65535, nullable=true)
     */
    private $imagelink;


    /**
     * @var float|null
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=true)
     */
    private $price;

    /**
     * @var string|null
     *
     * @ORM\Column(name="supplier", type="text", length=65535, nullable=true)
     */
    private $supplier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="web_site_supplier", type="text", length=65535, nullable=true)
     */
    private $webSiteSupplier;

  
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\SaleProduct", mappedBy="product")
     */
    private $saleProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductVariant", mappedBy="product")
     */
    private $productVariants;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BasketProduct", inversedBy="product")
     */
    private $basketProduct;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BasketItems", inversedBy="product")
     * @ORM\JoinColumn(nullable=false)
     */
    private $basketItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BagItems", inversedBy="product")
     */
    private $bagItems;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BasketProduct", inversedBy="product")
     */
    private $basketProducts;

  

    public function __construct()
    {
        $this->saleProducts = new ArrayCollection();
        $this->productVariants = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }
    public function getImagelink(): ?string
    {
        return $this->imagelink;
    }

    public function setImagelink(?string $imagelink): self
    {
        $this->imagelink= $imagelink;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSupplier(): ?string
    {
        return $this->supplier;
    }

    public function setSupplier(?string $supplier): self
    {
        $this->supplier = $supplier;

        return $this;
    }

    public function getWebSiteSupplier(): ?string
    {
        return $this->webSiteSupplier;
    }

    public function setWebSiteSupplier(?string $webSiteSupplier): self
    {
        $this->webSiteSupplier = $webSiteSupplier;

        return $this;
    }

    public function getSaleProduct(): ?SaleProduct
    {
        return $this->saleProduct;
    }

    public function setSaleProduct(?SaleProduct $saleProduct): self
    {
        $this->saleProduct = $saleProduct;

        return $this;
    }

    /**
     * @return Collection|SaleProduct[]
     */
    public function getSaleProducts(): Collection
    {
        return $this->saleProducts;
    }

    public function addSaleProduct(SaleProduct $saleProduct): self
    {
        if (!$this->saleProducts->contains($saleProduct)) {
            $this->saleProducts[] = $saleProduct;
            $saleProduct->setProduct($this);
        }

        return $this;
    }

    public function removeSaleProduct(SaleProduct $saleProduct): self
    {
        if ($this->saleProducts->contains($saleProduct)) {
            $this->saleProducts->removeElement($saleProduct);
            // set the owning side to null (unless already changed)
            if ($saleProduct->getProduct() === $this) {
                $saleProduct->setProduct(null);
            }
        }

        return $this;
    }
  public function __toString()
    {
      return sprintf( "%s", $this->getName());
    }

  /**
   * @return Collection|ProductVariant[]
   */
  public function getProductVariants(): Collection
  {
      return $this->productVariants;
  }

  public function addProductVariant(ProductVariant $productVariant): self
  {
      if (!$this->productVariants->contains($productVariant)) {
          $this->productVariants[] = $productVariant;
          $productVariant->setProduct($this);
      }

      return $this;
  }

  public function removeProductVariant(ProductVariant $productVariant): self
  {
      if ($this->productVariants->contains($productVariant)) {
          $this->productVariants->removeElement($productVariant);
          // set the owning side to null (unless already changed)
          if ($productVariant->getProduct() === $this) {
              $productVariant->setProduct(null);
          }
      }

      return $this;
  }

  public function getBasketProduct(): ?BasketProduct
  {
      return $this->basketProduct;
  }

  public function setBasketProduct(?BasketProduct $basketProduct): self
  {
      $this->basketProduct = $basketProduct;

      return $this;
  }

  public function getBasketItems(): ?BasketItems
  {
      return $this->basketItems;
  }

  public function setBasketItems(?BasketItems $basketItems): self
  {
      $this->basketItems = $basketItems;

      return $this;
  }

  public function getBagItems(): ?BagItems
  {
      return $this->bagItems;
  }

  public function setBagItems(?BagItems $bagItems): self
  {
      $this->bagItems = $bagItems;

      return $this;
  }

  public function getBasketProducts(): ?BasketProduct
  {
      return $this->basketProducts;
  }

  public function setBasketProducts(?BasketProduct $basketProducts): self
  {
      $this->basketProducts = $basketProducts;

      return $this;
  }

 

}
