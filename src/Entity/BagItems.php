<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BagItemsRepository")
 */
class BagItems
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="float")
     */
    private $totalSales;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $color;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="bagItems")
     */
    private $product;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateOfpurchase;

    public function __construct()
    {
        $this->product = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getTotalSales(): ?float
    {
        return $this->totalSales;
    }

    public function setTotalSales(float $totalSales): self
    {
        $this->totalSales = $totalSales;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProduct(): Collection
    {
        return $this->product;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
            $product->setBagItems($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getBagItems() === $this) {
                $product->setBagItems(null);
            }
        }

        return $this;
    }

    public function getDateOfpurchase(): ?\DateTimeInterface
    {
        return $this->dateOfpurchase;
    }

    public function setDateOfpurchase(?\DateTimeInterface $dateOfpurchase): self
    {
        $this->dateOfpurchase = $dateOfpurchase;

        return $this;
    }

    public function __toString()
    {
      return sprintf('%s', $this->getColor());
    }

}
