<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductVariantRepository")
 */
class ProductVariant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="productVariants")
     */
    private $product;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductOptionValue", mappedBy="optionValueVariant")
     */
    private $productOptionValues;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SaleProduct", inversedBy="productVariant")
     */
    private $saleProduct;

    public function __construct()
    {
        $this->productOptionValues = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Collection|ProductOptionValue[]
     */
    public function getProductOptionValues(): Collection
    {
        return $this->productOptionValues;
    }

    public function addProductOptionValue(ProductOptionValue $productOptionValue): self
    {
        if (!$this->productOptionValues->contains($productOptionValue)) {
            $this->productOptionValues[] = $productOptionValue;
            $productOptionValue->addOptionValueVariant($this);
        }

        return $this;
    }

    public function removeProductOptionValue(ProductOptionValue $productOptionValue): self
    {
        if ($this->productOptionValues->contains($productOptionValue)) {
            $this->productOptionValues->removeElement($productOptionValue);
            $productOptionValue->removeOptionValueVariant($this);
        }

        return $this;
    }

    public function getSaleProduct(): ?SaleProduct
    {
        return $this->saleProduct;
    }

    public function setSaleProduct(?SaleProduct $saleProduct): self
    {
        $this->saleProduct = $saleProduct;

        return $this;
    }

    function __toString()
    {
      return sprintf("%s", $this->getProduct()); 
    }
}
