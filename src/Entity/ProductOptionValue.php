<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductOptionValueRepository")
 */
class ProductOptionValue
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProductOptions", inversedBy="productOptionValues")
     */
    private $optionProduct;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProductVariant", inversedBy="productOptionValues")
     */
    private $optionValueVariant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function __construct()
    {
        $this->optionValueVariant = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOptionProduct(): ?ProductOptions
    {
        return $this->optionProduct;
    }

    public function setOptionProduct(?ProductOptions $optionProduct): self
    {
        $this->optionProduct = $optionProduct;

        return $this;
    }

    /**
     * @return Collection|ProductVariant[]
     */
    public function getOptionValueVariant(): Collection
    {
        return $this->optionValueVariant;
    }

    public function addOptionValueVariant(ProductVariant $optionValueVariant): self
    {
        if (!$this->optionValueVariant->contains($optionValueVariant)) {
            $this->optionValueVariant[] = $optionValueVariant;
        }

        return $this;
    }

    public function removeOptionValueVariant(ProductVariant $optionValueVariant): self
    {
        if ($this->optionValueVariant->contains($optionValueVariant)) {
            $this->optionValueVariant->removeElement($optionValueVariant);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
