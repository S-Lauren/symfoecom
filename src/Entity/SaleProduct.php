<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SaleProduct
 *
 * @ORM\Table(name="sale_product", indexes={@ORM\Index(name="sale_id", columns={"sale_id"}), @ORM\Index(name="product_id", columns={"product_id"})})
 * @ORM\Entity
  * @ORM\Entity(repositoryClass="App\Repository\SaleProductRepository")
 */
class SaleProduct
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var string|null
     *
     * @ORM\Column(name="color", type="text", length=65535, nullable=true)
     */
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="saleProducts")
     */
    private $product;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Sale", inversedBy="saleProducts")
     */
    private $sale;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProductVariant", mappedBy="saleProduct")
     */
    private $productVariant;

 

    public function __construct()
    {
        $this->product = new ArrayCollection();
        $this->productVariant = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getSale(): ?Sale
    {
        return $this->sale;
    }

    public function setSale(?Sale $sale): self
    {
        $this->sale = $sale;

        return $this;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->product->contains($product)) {
            $this->product[] = $product;
            $product->setSaleProduct($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->product->contains($product)) {
            $this->product->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getSaleProduct() === $this) {
                $product->setSaleProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ProductVariant[]
     */
    public function getProductVariant(): Collection
    {
        return $this->productVariant;
    }

    public function addProductVariant(ProductVariant $productVariant): self
    {
        if (!$this->productVariant->contains($productVariant)) {
            $this->productVariant[] = $productVariant;
            $productVariant->setSaleProduct($this);
        }

        return $this;
    }

    public function removeProductVariant(ProductVariant $productVariant): self
    {
        if ($this->productVariant->contains($productVariant)) {
            $this->productVariant->removeElement($productVariant);
            // set the owning side to null (unless already changed)
            if ($productVariant->getSaleProduct() === $this) {
                $productVariant->setSaleProduct(null);
            }
        }

        return $this;
    }


}
