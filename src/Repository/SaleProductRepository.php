<?php

namespace App\Repository;

use App\Entity\SaleProduct;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Knp\Component\Pager\PaginatorInterface;

/**
 * @method SaleProduct|null find($id, $lockMode = null, $lockVersion = null)
 * @method SaleProduct|null findOneBy(array $criteria, array $orderBy = null)
 * @method SaleProduct[]    findAll()
 * @method SaleProduct[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SaleProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, SaleProduct::class);
        $this->paginator = $paginator;

    }

    public function findByPage($page) {
      $dbquery =  $this->createQueryBuilder('s')
        ->getQuery();
        $pagination = $this->paginator->paginate($dbquery, $page, 100);
        return $pagination;
    }

    // /**
    //  * @return SaleProduct[] Returns an array of SaleProduct objects
    //  */
    
    public function findByColor($value, $page)
    {
      $query = $this->createQueryBuilder('s')
        ->where('s.color LIKE :val')
        ->setParameter('val', "%{$value}%")
        ->getQuery(); 
      $pagination = $this->paginator->paginate($query, $page, 5);
      return $pagination;
      
    }
    

    /*
    public function findOneBySomeField($value): ?SaleProduct
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
