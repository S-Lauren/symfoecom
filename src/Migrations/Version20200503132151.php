<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200503132151 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF1FA154 FOREIGN KEY (basket_items_id) REFERENCES basket_items (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADBEA49D43 FOREIGN KEY (bag_items_id) REFERENCES bag_items (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD20F2164B FOREIGN KEY (basket_products_id) REFERENCES basket_product (id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADF1FA154 ON product (basket_items_id)');
        $this->addSql('CREATE INDEX IDX_D34A04ADBEA49D43 ON product (bag_items_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD20F2164B ON product (basket_products_id)');
        $this->addSql('ALTER TABLE basket_product ADD color VARCHAR(255) NOT NULL, CHANGE quantity quantity INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE basket_product DROP color, CHANGE quantity quantity INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF1FA154');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADBEA49D43');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD20F2164B');
        $this->addSql('DROP INDEX IDX_D34A04ADF1FA154 ON product');
        $this->addSql('DROP INDEX IDX_D34A04ADBEA49D43 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD20F2164B ON product');
    }
}
