<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200502141323 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_variant ADD sale_product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_variant ADD CONSTRAINT FK_209AA41DB8D0821E FOREIGN KEY (sale_product_id) REFERENCES sale_product (id)');
        $this->addSql('CREATE INDEX IDX_209AA41DB8D0821E ON product_variant (sale_product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_variant DROP FOREIGN KEY FK_209AA41DB8D0821E');
        $this->addSql('DROP INDEX IDX_209AA41DB8D0821E ON product_variant');
        $this->addSql('ALTER TABLE product_variant DROP sale_product_id');
    }
}
