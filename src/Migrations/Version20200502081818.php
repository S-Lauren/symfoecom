<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200502081818 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_option_value (id INT AUTO_INCREMENT NOT NULL, option_product_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_A938C737A8ED7E6A (option_product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_option_value_product_variant (product_option_value_id INT NOT NULL, product_variant_id INT NOT NULL, INDEX IDX_42D34433EBDCCF9B (product_option_value_id), INDEX IDX_42D34433A80EF684 (product_variant_id), PRIMARY KEY(product_option_value_id, product_variant_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_variant (id INT AUTO_INCREMENT NOT NULL, product_id INT DEFAULT NULL, INDEX IDX_209AA41D4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_options (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_option_value ADD CONSTRAINT FK_A938C737A8ED7E6A FOREIGN KEY (option_product_id) REFERENCES product_options (id)');
        $this->addSql('ALTER TABLE product_option_value_product_variant ADD CONSTRAINT FK_42D34433EBDCCF9B FOREIGN KEY (product_option_value_id) REFERENCES product_option_value (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_option_value_product_variant ADD CONSTRAINT FK_42D34433A80EF684 FOREIGN KEY (product_variant_id) REFERENCES product_variant (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_variant ADD CONSTRAINT FK_209AA41D4584665A FOREIGN KEY (product_id) REFERENCES product (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_option_value_product_variant DROP FOREIGN KEY FK_42D34433EBDCCF9B');
        $this->addSql('ALTER TABLE product_option_value_product_variant DROP FOREIGN KEY FK_42D34433A80EF684');
        $this->addSql('ALTER TABLE product_option_value DROP FOREIGN KEY FK_A938C737A8ED7E6A');
        $this->addSql('DROP TABLE product_option_value');
        $this->addSql('DROP TABLE product_option_value_product_variant');
        $this->addSql('DROP TABLE product_variant');
        $this->addSql('DROP TABLE product_options');
    }
}
