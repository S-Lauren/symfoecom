<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{

    public function __construct(UserPasswordEncoderInterface $pswEncoder) {

      $this->pswEncoder = $pswEncoder; 

    }
    
      
    

    public function load(ObjectManager $manager)
    {
      foreach ($this->getUserData() as [$username, $password, $roles]) {
    
          $user = new User(); 
          $user->setUsername($username); 
          $user->setPassword($this->pswEncoder->encodePassword($user, $password));
          $user->setRoles($roles);
          $manager->persist($user);
      }

      $manager->flush();
    }


    private function getUserData(): array

    {
      return 
      [
          ['Georges Abitbol', 'georges123', ['ROLE_ADMIN']],
          ['McGallagan', 'userlambda', ['ROLE_USER']]
      ]; 
    }
 
}
