<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Product;
use App\Entity\SaleProduct;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
/**
  * @Route("/admin")
 * Require ROLE_ADMIN for *every* controller method in this class.
  *
  * @IsGranted("ROLE_ADMIN")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
     
      
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository(Product::class)->findAll();

        return $this->render('product/index.html.twig', [
            'products' => $products,
        ]);
      }
    
   
    /**
     * @Route("/save_product", name="save_product")
     */
    public function saveProduct(Request $request) {
      $em = $this->getDoctrine()->getManager();
      $productName = $request->get("productname"); 
      $description = $request->get("description"); 
      $price = $request->get("price"); 
      $supplier = $request->get("supplier"); 
      $website = $request->get("website"); 
      $color = $request->get("color"); 
      $quantity = $request->get("quantity"); 

      $product = new Product(); 
      $saleProduct = new SaleProduct();
      $product->setName($productName);
      $product->setDescription($description);
      $product->setPrice($price);
      $product->setSupplier($supplier);
      $product->setWebSiteSupplier($website);
      $product->addSaleProduct($saleProduct->setColor($color)->setQuantity($quantity)); 
      $em->persist($saleProduct);
      $em->persist($product);
      $em->flush(); 
      return $this->redirectToRoute("product");
    }


    public function filterBy() {

        // trier par color 
    }

}
