<?php

namespace App\Controller;

use App\Entity\BagItems;
use App\Entity\Product;
use App\Entity\BasketProduct;
use App\Entity\ProductOptions;
use App\Entity\ProductVariant;
use App\Entity\ProductOptionValue;
use DateTime;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class FrontController extends AbstractController
{
    /**
     * @Route("/front", name="front")
     */
    public function index()
    {
        $productList = $this->getDoctrine()->getRepository(Product::class)->findAll(); 
        $prods = [];
          dump($prods);
        return $this->render('front/index.html.twig', [
          'prod' => $productList,
        ]);
    }
    /**
     * @Route("/product/{id}", name="view_product")
     */

    public function viewProduct($id, Request $request) {

    $em = $this->getDoctrine()->getManager(); 
    $prod = $this->getDoctrine()->getRepository(Product::class)->find($id);  
// $prod1 = $this->getDoctrine()->getRepository(Product::class)->find(1);  
    // fetch the option... 
    $variantProd = $this->getDoctrine()->getRepository(ProductVariant::class)->find($id);
    $r = $variantProd->getProductOptionValues(); 
    dump($variantProd); 


    // //   foreach ($r as $key => $val) {
    // //  dump($val);
    // // }
    //   $basket = new BasketProduct(); 
    //   $basket->addProduct($prod); 

    //   // retrieve data.
    //   $quantity = $request->request->get("quantity");
    //   $option = $request->request->get("color");
    // dump($quantity);
    //   dump($option); 
    //   // $basket->setColor($bagcolor); 
    //   $basket->setQuantity($quantity);
    //   $basket->addProduct($prod);
    //   $basket->setTotalSales($quantity*$prod->getPrice());
    //   $em->persist($basket); 
    //   $em->flush(); 

    return $this->render('front/product_view.html.twig', [
          'prod' => $prod,
          'variant'=>$r,
        // 'basket'=>$basket,
        ]);
    }


    /**
     * @Route("/save_basket/{id}", name="save_basket")
     */
    public function basketItems(Request $request, $id)

    {     

    $em = $this->getDoctrine()->getManager(); 
      $prod = $this->getDoctrine()->getRepository(Product::class)->find($id);  
       $basket = new BasketProduct(); 
      $basket->addProduct($prod); 

      // retrieve data.
      $quantity = $request->request->get("quantity");
       $option = $request->request->get("color");

      $date= new DateTime();      
        dump($option);
      // dump($option); 
      $basket->setColor($option); 
      $basket->setQuantity($quantity);
      $basket->setDate($date);
      $basket->addProduct($prod);
      $basket->setTotalSales($quantity*$prod->getPrice());
      $em->persist($basket); 
      $em->flush(); 
  return $this->redirectToRoute("show_basket");
        
    }

    /**
     * @Route("/show", name="show_basket")
     */
    public function showBasket()
    {
      $em = $this->getDoctrine()->getManager(); 
      
      $basket = $this->getDoctrine()->getRepository(BasketProduct::class)->findAll(); 
    
      return $this->render("front/basket_view.html.twig", [

      'basket' => $basket,
     
        ]);
    }


    /**
     * @Route("/", name="home")
     */
    public function home()
    {
      return $this->redirectToRoute("app_login");
    }
}
