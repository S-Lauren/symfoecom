<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Repository\CustomerRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

    /**
     * @Route("/admin")
     */
class CustomerController extends AbstractController
{
    /**
     * @Route("/customer", name="customer")
     */
    public function index()
    {
      $this->denyAccessUnlessGranted('ROLE_ADMIN');
      $em = $this->getDoctrine()->getManager();
      $customers = $em->getRepository(Customer::class)->findAll();
        return $this->render('customer/index.html.twig', [
          'customers' => $customers,
        ]);
    }
    /**
     * @Route("/search_customer", name="search_customer")
     */
    public function searchByFirstName(Request $request) {
      $em = $this->getDoctrine()->getManager();
      $keyword = $request->get('firstname'); 
      $customers = $em->getRepository(Customer::class)->searchBy($keyword);
  
      return $this->render('customer/index.html.twig', [
          'customers' => $customers,
      ]);
    }

    /**
     * @Route("/edit/{id}", name="edit-customer")
     */
    public function saveCustomer(Request $request, $id) {
      $em = $this->getDoctrine()->getManager();
      $customers = $em->getRepository(Customer::class)->find($id);
      return $this->render('customer/customer_edit.html.twig', [
          'customers' => $customers,
      ]);
    }
    /**
     * @Route("/save_customer/{id}}", name="save_customer")
     */
    public function editCustomer(Request $request,$id) {

   // Retrieve datas from form
      $em = $this->getDoctrine()->getManager();
      $customer = $em->getRepository(Customer::class)->find($id);
      $firstname = $request->get("firstname");
      $lastname = $request->get("lastname");
      $country= $request->get("country");
      $mail= $request->get("mail");
      $city = $request->get("city");
      $address= $request->get("address");

      // send to Entity customer
      $customer->setFirstName($firstname);
      $customer->setLastName($lastname);
      $customer->setCountry($country);
      $customer->setMail($mail);
      $customer->setAddress($address);
      $customer->setCity($city);

      $em->persist($customer);
      $em->flush();

      return $this->render('customer/customer_edit.html.twig', [
          'customer' => $customer,
      ]);
    }
}
