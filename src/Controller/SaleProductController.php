<?php

namespace App\Controller;

use App\Entity\Sale;
use App\Entity\SaleProduct;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
  /**
  * @Route("/admin")
  */
class SaleProductController extends AbstractController
{
    /**
     * @Route("/sale-product/{page}", defaults={"page":"1"}, name="sale_product")
     */
    public function index($page ,Request $request)
    {
      $em = $this->getDoctrine()->getManager();
    
      $saleProducts = $em->getRepository(SaleProduct::class)->findBypage($page);

        return $this->render('sale_product/index.html.twig', [
            'sales' => $saleProducts,

        ]);
    }

    /**
     * @Route("/find-by-color/{page}", defaults={"page":"1"}, name="findbycolor")
     */

    public function findColor(Request $request, $page)
    {
        $em = $this->getDoctrine()->getManager();

        $blue = $request->get('color');
        $saleProducts = $em->getRepository(SaleProduct::class)->findByColor($blue, $page);

        return $this->render('sale_product/index.html.twig', [
            'sales' => $saleProducts,
        ]);
    }
}
